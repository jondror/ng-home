import { Injectable } from '@angular/core';
import {Http} from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/delay';
import {AngularFire} from 'angularfire2';

@Injectable()
export class PostsService {

  postsObservable;

  getPosts(){
    this.postsObservable = this.af.database.list('/posts').map(
      posts =>{
        posts.map(
          post => {
            post.userNames = [];
            for (var u in post.users){
              post.userNames.push(
                this.af.database.object('/users/'+u)
              )
            }
          }
        );
        return posts;
      }
    )
    return this.postsObservable;
  }

  addPost(post){
    this.postsObservable.push(post);
  }

  deletePost(post){
    this.af.database.object('/posts/'+post.$key).remove();
  }

  updatePost(post){
  let postData = {title:post.title, author:post.author, body:post.body};
  this.af.database.object('/posts/'+post.$key).update(postData);
}

  constructor(private af:AngularFire) { } //dependency injection

}
