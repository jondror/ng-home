import { Component, OnInit } from '@angular/core';
import {PostsService} from './posts.service';

@Component({
  selector: 'jce-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit {

  posts;
  isLoading:Boolean = true;

  constructor(private _postService:PostsService) { }

deletePost(post){
    this._postService.deletePost(post);
  }

addPost(post){
  this._postService.addPost(post);
}

updatePost(post){
  this._postService.updatePost(post);
}

  ngOnInit() {
    this._postService.getPosts().subscribe(postsData => {
      this.posts = postsData;
      this.isLoading = false;

    });
  }

}
