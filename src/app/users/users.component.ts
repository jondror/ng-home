import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'jce-users',
  templateUrl: './users.component.html',
  styles: [`
    .users li { cursor: default; }
    .users li:hover { background: #ecf0f1; } 
  `]
})
export class UsersComponent implements OnInit {

  users = [
    {name: 'Jon', email:'jon@dror.io'},
    {name: 'Tal', email:'tal@barashi.com'},
    {name: 'Geoffrey', email:'geoffrey@clean.com'}

  ]
  
  constructor() { }
  addUser(user){
    this.users.push(user);
  }

  ngOnInit() {
  }

}
