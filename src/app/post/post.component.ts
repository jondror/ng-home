import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import {Post} from './post';

@Component({
  selector: 'jce-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css'],
  inputs: ['post']
})
export class PostComponent implements OnInit {

  post:Post;
  @Output() deleteEvent = new EventEmitter<Post>();
  @Output() editEvent = new EventEmitter<Post>();
  isEdit:Boolean = false;

  editButtonText:string="Edit";

  oldPost:Post;

  constructor() { }

  sendDelete(){
    this.deleteEvent.emit(this.post);
    //create a delete event and emit (send) the post data.
  }

  toggleEdit(){
    this.isEdit = !this.isEdit;
    this.isEdit ? this.editButtonText="Save" : this.editButtonText="Edit";
    if(this.isEdit){
      this.oldPost=Object.assign({},this.post);
    }
    else{
      this.editEvent.emit(this.post);
    }
  }

  cancelEdit(){
    console.log("Cancel init");
    console.log("Old Post is: "+this.oldPost.title);
    console.log("New Post is: "+this.post.title);
    this.post=this.oldPost;
    this.toggleEdit();
  }
  


  ngOnInit() {
  }

}
