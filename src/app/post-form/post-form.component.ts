import {NgForm} from '@angular/forms';
import {Post} from '../post/post';
import {Component, OnInit, Output, EventEmitter} from '@angular/core';


@Component({
  selector: 'jce-post-form',
  templateUrl: './post-form.component.html',
  styleUrls: ['./post-form.component.css']
})
export class PostFormComponent implements OnInit {
  post:Post = {title:'', author:'', body:''};
  @Output() postAddedEvent = new EventEmitter<Post>();

  constructor() { }

  onSubmit(form:NgForm){
    console.log(form);
    this.postAddedEvent.emit(this.post);
    this.post = {
      title:'',
      author:'',
      body:''
    }

  }

  ngOnInit() {
  }

}
